#!/bin/bash -eu
docker network create rainbow-net

for color in red orange yellow green blue purple
do
	echo $color
    docker run --detach --name=$color --network rainbow-net httpd
    echo "<html><body><h1>$color</h1></body></html>" > $color.html
    docker cp $color.html $color:/usr/local/apache2/htdocs/index.html
done

docker run --detach --name=rainbow --publish 80:80 \
    --volume $PWD/haproxy/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg --network rainbow-net \
    haproxy
