# Faster Stacks with Docker

## The SFS Rap

[wrapper slides](wrapper-slides.fodp)

---

## Talk 0: The Arc of History: PMs, VMs, Containers

A tale of efficiency and ephemerality in troubled times.

*once upon a time...*
![image](1_aghi2docker-YeOlde.png?)

---
*times keeps on slippin'...*
![image](2_aghi2docker-complexity.png?)

---

![image](3_aghi2docker_P2V.png?)

---

![image](4_aghi2docker-vms_containers.png?)

* efficiency + less storage space = greater node density

* containers are made to spin up/down very fast = e-commerce, quick simulations

* containers are more portable than VMs

    - run any where Docker can be installed
    - containers + registries + API + cloud = perfect storm to shorten SW Dev Life Cycles (DevOps)

![image](container-mobility.png?)

---

## Exercise 0: Get Reqd

For 2020-03-21 delivery, we will use VMs in AWS.

ssh to the provided IP address as sfs-alice or sfs-bob

```bash
ssh sfs-alice@44.234.8.97
```

This is a bare CentOS 7 machine. Run the following commands.

Install git and fetch in the class materials:

```bash
curl -O https://www.sofree.us/bits/git-a-class
chmod +x git-a-class
./git-a-class aghi2docker
```

Install Docker

```bash
curl -fsSL https://get.docker.com/ | sh
```

If needed, enable the current user to run Docker commands without sudo.

```bash
sudo usermod -aG docker $USER && exit
```

If needed, add the docker group, and restart the service

```bash
sudo groupadd docker
sudo systemctl restart docker
```

If needed, start and enable the service

```bash
sudo systemctl start docker
sudo systemctl enable docker
```

Test Docker

```bash
docker run --rm hello-world
```

## Exercise 1: Testing a shell script for portability.

- Docker-based R&D is fast AF
- Dockerfile quality matters very much: Keep it simple. Like, no Dockerfile at all, most of the time.

Say you're one of the teachers in a Software Freedom School. You'd like to make a script that downloads class materials from git into a known directory structure, so that you can offer specific instructions like:

```bash
cd
cd sfs/pipeline-play
less README.md
```

And, you can trust that they'll work consistently on all students' machines.

The class downloader needs to run on all the Linux. So, let's test it on enough of them that we're confident it will.

```bash
docker run -it ubuntu /bin/bash
apt update && apt -y install wget
wget https://www.sofree.us/bits/git-a-class
chmod +x git-a-class
./git-a-class pipeline-play
```

```bash
docker run -it fedora /bin/bash
curl -O https://www.sofree.us/bits/git-a-class
chmod +x git-a-class
./git-a-class pipeline-play
```

```bash
docker run -it opensuse/leap /bin/bash
zypper install -y curl
curl -O https://www.sofree.us/bits/git-a-class
chmod +x git-a-class
./git-a-class pipeline-play
```

That's probably enough for now. The nice thing about this way of testing for portability across distributions is that it only takes a few minutes per distro, where doing the same thing in vagrant takes *much* longer.

---

## Exercise 2: Web servers one two

Let's say you want a simple web-server, nothing in particular, just something that returns something, so you can test a firewall, or a load-balancer, or whatever.

```bash
docker run --name testy --detach httpd
docker inspect testy | grep IPA
```

Now, `curl` that IP.

And it works just the same with nginx...

```bash
docker rm -f testy
docker run --name testy --detach nginx
docker inspect testy | grep IPA
```

Now, let's try browsing GUIly. Add `--publish 80:80` to your launch expression.

```bash
docker rm -f testy
docker run --name testy --detach --publish 80:80 nginx
```

Now, `xdg-open`, or manually point your web-browser at the *public* IP of your host.

## Exercise 3: Wordpress on Docker. That's pretty, fast.

But really, let's say you're trying to help your friend/relative create a *pretty* web-site. And, web-dev is *hard*. And, one only has so much time available. Maybe this is a job for Wordpress? Let's give that a swing...

```bash
docker network create wordpress-net
docker run --detach --network wordpress-net \
    -e MYSQL_RANDOM_ROOT_PASSWORD=true \
    --name wordpress-db \
    mariadb
docker run --detach --network wordpress-net \
    --name wordpress-app \
    --publish 80:80 \
    wordpress

docker inspect wordpress-app | grep IPA

docker logs wordpress-db 2>&1 | grep ROOT
# if that didn't return anything, wait, like this:
until docker logs wordpress-db 2>&1 | grep ROOT
do
  echo -n .
  sleep 1
done
```

Use `xdg-open` or manually direct your browser to wordpress-app's IP Address and connect the -app to the -db. Note that putting the machines on a shared network let them address one another by name.

[example first config screen](wordpress-config-example.png)

---

# Exercise 4: Prometheus and friends

Suppose that you want to learn to use Prometheus. You quickly find that other things are needed, like Grafana. You do a little looking up, so you can separate important config from not-so-important data. You write a nice little script to yoink the stock config from fresh containers: `pave`. You write a nice little script to blast away the things you've yoinked: `nuke`. You discard a *lot* of manual startup scripts, realizing that `docker-compose` is a fresh and flowery way to accomplish the same things you've been doing with them.

```bash
cd ~/sfs/aghi2docker/monitoring/
./pave
sudo curl -L \
  "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" \
  -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose up -d
# or docker-compose up with no d if you want to watch logs
```

View Prometheus' own metrics

```bash
curl localhost:9090/metrics 2>/dev/null | grep -C3 http_requests
```

In the browser, connect Grafana to Prometheus.

- default Grafana login is admin:admin
- add a Prometheus data-source. HTTP URL is http://prometheus:9090
  Thank goodness for free name-resolution on a dedicated network, right?
- do more clever things here as you learn Grafana and Prometheus (and Alertmanager and Blackbox exporter)

Example PromQL queries:
- prometheus_http_requests_total
- rate(prometheus_http_requests_total[1m])
- go_memstats_gc_cpu_fraction
- rate(go_memstats_alloc_bytes_total[5m])

# Exercise 5: Rainbow (WIP)

WIP in rainbow/

# Exercise 7?: Nethack

You've worked hard. Play a little!

```bash
docker run -it matsuu/nethack
```

Or, if you prefer to suffer some more...

https://github.com/bencawkwell/dockerfile-dwarffortress
