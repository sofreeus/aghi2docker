`aws-create-ansible-controller.yml` creates a Fedora 31 machine

Requires:
- awscli and much boto installed
  * ubuntu:
    ```bash
    sudo apt -y install python{,3}-boto{,3,core}
    ```
  * fedora:
    ```bash
    dnf -y install ansible awscli python3-boto3 python3-boto
    ```
- awscli credentials configured and using us-west-2
  ```bash
  aws configure --profile=sofreeus
  export AWS_PROFILE=sofreeus
  ```

After running `aws-create-ansible-controller.yml`, manually add the DNS record at Gandi:

`ansible-controller.sofree.us A 123.45.67.90`

Update admins and users in `host_vars/ansible-controller.sofree.us.yml` then run:

1. `add-linux-admins.sh` to add Linux admins
2. `update-linux-admins.sh` to add new and remove old
3. `update-linux-users.sh` to add Linux users

## TODO

Add Ansible, AWS CLI, and whatever else is needed
Add ec2-admin settings to /etc/skel/
Add clone of repo to /etc/skel/?
