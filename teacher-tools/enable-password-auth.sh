#!/bin/bash -eu

sed -e '/^PasswordAuthentication no/d' -i /etc/ssh/sshd_config
sed -e '/^ChallengeResponseAuthentication no/d' -i /etc/ssh/sshd_config
systemctl restart sshd
